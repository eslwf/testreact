import React,{Fragment,useState} from "react";
const InputTodo = ()=>{

    const [description,setDescription] = useState("") //useState("hello") will show the default hello in the text
    const onSubmitForm = async (e)=>{
        e.preventDefault()
        try{
            const body = {description}
            const res = await fetch("http://172.104.42.214:4006/todos",{
                method : "POST",
                headers : {"Content-Type":"application/json"},
                body : JSON.stringify(body)
            })
            window.location = "/" //refresh the page
        }
        catch(err){
            console.error(err.message)
        }
    }
    return(
    <Fragment>
        <h1 className="text-center mt-5">Todo List</h1>
        <form className="d-flex mt-5" onSubmit={onSubmitForm}>  {/* put the text and add tgt*/}
            <input text="text" className="form-control" value={description} onChange={input => setDescription(input.target.value) }/> 
            {/* onChange allows to change the value in the text*/ }
            <button className="btn btn-success">Add</button>
        </form>
    </Fragment>
    )
}

export default InputTodo