const express = require("express")
const app = express()
const pool = require("./db")
// const rest = require("./component/REST").rest;
const cors = require("cors")
app.use(cors())
app.use(express.json()) //get user req

//create a todo
app.post("/todos",async(req,res)=>{
    try{ 
        console.log("req.body",req.body)
        const newTodo = await pool.query(
            `INSERT INTO todo (description) VALUES($1) RETURNING *`, 
            //$1 refers to the first argument, $2 to the second
            //RETURNING * will return back the data in the res
            [req.body.description] //it takes an array in the insert
        )
        res.json(newTodo.rows[0]) //.rows[0] : allow to view the updated value 
    } 
    catch(err){
        console.error(err.message)
    }
})

//get all the todo
app.get("/todos",async(req,res)=>{
    try{
        const allTodos = await pool.query("SELECT * FROM todo");
        res.json(allTodos.rows)
    }
    catch(err){
        console.error(err)
    }
})

//get one todo
app.get("/todos/:id",async(req,res)=>{
    try{
        const {id} = req.params //allow to get the id
        const todo = await pool.query("SELECT * FROM todo WHERE todo_id = $1", [id])
        res.json(todo.rows[0]) 
    } 
    catch(error){
        console.error(error.message)
    }
})


//update a todo
app.put("/todos/:id",async(req,res)=>{
    try{
        const {id}= req.params
        const {description} = req.body
        const  updateTodo = await pool.query(
            "UPDATE todo SET description = $1 WHERE todo_id = $2",[description,id])
        res.json("Todo was updated")
    }
    catch(error){
        console.error(error.message)
    }
})

//delete a todo
app.delete("/todos/:id",async(req,res)=>{
    try{
        const {id} = req.params
        const deleteTodo = await pool.query(
            "DELETE FROM todo WHERE todo_id = $1",
            [id]
        )
        res.json("Todo was deleted")
    }
    catch(err){
        console.error(err.message)
    }
})


app.listen(4006,()=>{
    console.log("Connected port 4006")
})