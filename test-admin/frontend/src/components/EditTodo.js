import React,{Fragment,useState} from "react"
const EditTodo= ({todo})=>{
    const [description,setDescription] = useState(todo.description)

    const updateDes =async(e)=>{
        try{
            const body = {description}
            const res = await fetch(`http://172.104.42.214:4006/todos/${todo.todo_id}`,{
                method : "PUT",
                headers : {"Content-Type": "application/json"},
                body : JSON.stringify(body)
            })
            window.location = "/"
        }
        catch(err){
            console.error(err.message)
        }
    }

    return (
        <Fragment>
        <button type="button" class="btn btn-warning" data-bs-toggle="modal" data-bs-target={`#id${todo.todo_id}`}>
        Edit
        </button>

       {/* id = id10, make sure the model is unique */}
        <div class="modal fade" id={`id${todo.todo_id}`} tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit todo</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" onClick={()=>setDescription(todo.description)} ></button>
            </div>
            <div class="modal-body"   >
                <input type='text' className="form-control" value={description} onChange ={e=>{
                    setDescription(e.target.value)
                }}/>
                {/* onChange allows u to change description  */}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-bs-dismiss="modal" onClick= {e=>updateDes(e)}>Edit</button>
                <button type="button" class="btn btn-danger" data-bs-dismiss="modal" onClick={()=>setDescription(todo.description)}>Close</button>
  
            </div>
            </div>
        </div>
        </div>
        </Fragment>
    )
}
export default EditTodo