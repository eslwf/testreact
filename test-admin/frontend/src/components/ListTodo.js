import React,{Fragment,useEffect,useState}  from "react";
import EditTodo from "./EditTodo";

const ListTodo = () =>{
    const [todos,setTodos]= useState([]);

    const getTodos = async()=>{
    try{
        const res =await fetch("http://172.104.42.214:4006/todos")
        const jsonData = await res.json()
        setTodos(jsonData)
    }
    catch(err){  
        console.error(err.message)
    }
    }    
    useEffect(()=>{ //keeps on making req
        getTodos()
    },[]) //use [] can only make one req
   

    const deleteTodo = async (id)=>{
        try{
            const deleteTodo = await fetch(`http://172.104.42.214:4006/todos/${id}`,{
                method :"DELETE"
            })

           setTodos(todos.filter(todo=>todo.todo_id !== id)) //filter: if elements match the condition, will return those elements
        }

        catch(err){
            console.error(err.message)
        }
    }    
    return ( 
        <Fragment>
            <div class="container">
            <h2 class="mt-5">List Todo Table</h2>
            <table class="table mt-5 text-center">
                <thead>
                
                
                <tr> 
                    <th>Description</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                {todos.map(todo=>(
               
                    <tr key={todo.todo_id}>{/*Avoid it shows "each child in a list should have unique key" */}
                        <td>{todo.description}</td>
                        <td><EditTodo todo={todo}/></td>
                        <td><button className="btn btn-danger" onClick={()=>deleteTodo(todo.todo_id)}>Delete</button></td>
                    </tr>
               ))}
                </tbody>
            </table>
            </div>
        </Fragment>
        
    )
}

export default ListTodo